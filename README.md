# Jr. Back-end Assessment API
This API is for assessment purpose only. 

## **Feature**
-   User Register
-   User login
-   Get user data
-   Find duplicate number


##  **Tech Stack**
-   Nodejs v10.19.0
-   Bcrypt 5.0.0
-   Jsonwebtoken 8.5.1
-   PosgreSQL
-   Heroku
-   sequelize
-   Jest & supertest

## **How to run this API**
**preparation**
- npm install 
- sudo apt-get Postgresql
- fill out the .env file with yours (for the format see .env.example)
- sequelize db:create
- sequelize db:migrate


**running**
1. npm run dev
2. Hit the endpoint with your software development tool such as Postman

## **Testing**
- sequelize db:create --env test
- sequelize db:migrate --env test
- Npm run test

For the endpoint documentation(Postman) [Click Here](https://documenter.getpostman.com/view/11673922/T1LJnUww?version=latest)

For the API Deploy URL (Heroku) [Click Here](https://heloworld-assessment.herokuapp.com/)
