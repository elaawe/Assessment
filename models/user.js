'use strict';
const bcrypt = require('bcrypt');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  User.init({
    fullname: {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'Fullname must be filled'
      }
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true,
      validate: {
        isEmail: {
          args: true,
          msg: 'Must be filled with email'
        },
        async isExist(value) {
          const result = await User.findOne({ where: { email: value } })
          if (result != null && result.email == value)
            throw new Error('Email already exist. Use another one or login')
        }

      }
    },
    password:  {
      type: DataTypes.STRING,
      allowNull: {
        args: false,
        msg: 'password must be filled'
      },
      validate:{
        min: 6
      }
    }
  },
  {
    sequelize,
    modelName: 'User',
  })

  Object.defineProperty(User.prototype, 'entity', {
    get() {
      return {
        id: this.id,
        email: this.email,
        fullname: this.fullname
      }
    }
  })
;
  return User;
};