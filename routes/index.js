const router = require('express').Router()
const user = require('./user')
const duplicated = require('./duplicated')


router.use('/dup',duplicated)
router.use('/user', user)

module.exports = router

