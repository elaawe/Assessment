const router = require('express').Router()
const dup = require('../controllers/findSameNumber')

router.post('/number', dup.sameNumber)

module.exports = router