const app = require('../app');
const port = process.env.PORT || 1708


app.listen(port, function () {
  console.log(`listening on ${port}`);
});