module.exports = {
    success: (statusCode,result) => ({
        status: true,
        httpCode: statusCode,
        result
    }),

    error: (statusCode, err) => ({
        success: statusCode,
        message: err.message
    })

}